package com.teosoft.date;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class XDateTest {
  @Test(dataProvider = "dp")
  public void testXDate(int days, XDate initial, XDate expected) {
	  assertEquals(initial.addDays(days), expected);
//	  System.out.printf("days: %d, initial: %s, expected: %s%n", days, initial, expected);
  }

  @DataProvider
  public Object[][] dp() {
    return new Object[][] {
      new Object[] {0, new XDate(0,0,0), new XDate(0,0,0)},
      new Object[] {1, new XDate(0,0,0), new XDate(0,0,1)},
      new Object[] {32, new XDate(0,0,0), new XDate(0,1,1)},
      new Object[] {31+28, new XDate(0,0,0), new XDate(0,2,0)},
      new Object[] {31+27, new XDate(0,0,0), new XDate(0,1,27)},
      new Object[] {365, new XDate(0,0,0), new XDate(1,0,0)},
      new Object[] {366, new XDate(0,0,0), new XDate(1,0,1)},
      new Object[] {365, new XDate(1,0,0), new XDate(2,0,0)},
    };
  }
}
