package com.teosoft.date;

public class XDate {

	public XDate(int year, int month, int dayOfMonth) {
		this.year = year;
		this.month = month;
		this.dayOfMonth = dayOfMonth;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dayOfMonth;
		result = prime * result + month;
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XDate other = (XDate) obj;
		if (dayOfMonth != other.dayOfMonth)
			return false;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "XDate [year=" + year + ", month=" + month + ", dayOfMonth=" + dayOfMonth + "]";
	}

	public final int daysInMonth[]= {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	public final int year;
	public final int month;
	public final int dayOfMonth;
	
	public XDate addDays(int days) {
		int y = this.year;
		int m = this.month;
		int d = this.dayOfMonth + days;
		while(d > 0) {
			if(d >= daysInMonth[m]) {
				d -= daysInMonth[m];
				m += 1;
				if(m > 11) {
					m = 0;
					y += 1;
				}
			}
			else
				break;
		}
		return new XDate(y, m, d);
	}
	
}
